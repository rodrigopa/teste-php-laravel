<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.min.css" integrity="sha512-xiunq9hpKsIcz42zt0o2vCo34xV0j6Ny8hgEylN3XBglZDtTZ2nwnqF/Z/TTCc18sGdvCjbFInNd++6q3J0N6g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Importar</title>
    <style>
        html, body {
            height: 100%;
        }
        .main {
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
</head>
<body>
    <div class="main">
        <div>
            @if (session('success'))
                <blockquote style="display: block">
                    <p>
                        <em>
                            <strong>Sucesso!</strong><br />
                            Os documentos foram importados com sucesso.
                        </em>
                    </p>
                </blockquote>
            @endif
            <form method="post" action="">
                {{csrf_field()}}
                <button>Importar documentos</button>
            </form>
        </div>
    </div>
</body>
</html>
