### Run docker compose
```docker compose up```

### Run fresh migrations
```docker exec teste-php-laravel-php-fpm php artisan migrate:fresh```

### Run categories seeders
```docker exec teste-php-laravel-php-fpm php artisan db:seed --class CategorySeeder```


