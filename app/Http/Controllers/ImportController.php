<?php

namespace App\Http\Controllers;

use App\Jobs\ImportDocument;
use Illuminate\Support\Facades\Redis;

class ImportController extends Controller
{
    public function import()
    {
        $file = file_get_contents(storage_path('data/2023-03-28.json'));
        $data = json_decode($file);
        $documents = $data->documentos;

        foreach ($documents as $document) {
            ImportDocument::dispatch($document);
        }

        return redirect('/import')->with('success', true);
    }

    public function index()
    {
        return view('import');
    }
}
