<?php

namespace App\Jobs;

use App\Models\Category;
use App\Models\Document;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportDocument implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(readonly object $document)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $category = Category::where('name', $this->document->categoria)
                ->first();

            Document::query()->create([
                'category_id' => $category->id,
                'title' => $this->document->titulo,
                'contents' => $this->document->{'conteúdo'}
            ]);
        } catch (\Exception $e) {
            Log::info($e);
        }
    }
}
